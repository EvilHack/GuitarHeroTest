﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class DetectarNota : MonoBehaviour
{
  public  KeyCode tecla;
    GameManager gameManager;
    Animator animator;
    public AudioClip onTouchNoteSound;
    public AudioClip onFailNoteSound;
    public ParticleSystem touchEffect;
    bool touchedInTime = false;
    SoundManager soundManager;
    // Start is called before the first frame update
    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        animator = GetComponentInParent<Animator>();
        soundManager = FindObjectOfType<SoundManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        reallyTouched = false;
        Debug.Log("Esto va 2");
       reallyTouched=  CheckNotes(collision.gameObject);
    }
    bool reallyTouched=false;
      private void OnTriggerStay2D(Collider2D collision)
    {

        if (!touchedInTime && !reallyTouched) {

         reallyTouched=  CheckNotes(collision.gameObject);
        }

    }


    public bool CheckNotes(GameObject col) {
        bool inputResult;
        if (gameManager.easyMode) inputResult = Input.GetKey(tecla);   
        else inputResult = Input.GetKeyDown(tecla);   
        if (inputResult    &&!touchedInTime ) {
            Debug.Log("Nota  no tocada aun");
         
            animator.SetBool("noteInRange", true);
            touchEffect.Stop();
            touchEffect.startColor = Color.green;
            touchEffect.Play();
            if (col.tag.Equals("Note")&&!touchedInTime)
            {
                Debug.Log("Nota tocada");
 
                soundManager.PlaySE(onTouchNoteSound,1);
                gameManager.RemoveNote();
                gameManager.AddScore(col.GetComponent<NoteBehaviour>().score);
                gameManager.squaresHitted++;
                touchedInTime = true;
                reallyTouched = true;
                Destroy(col.gameObject);
                return true;
            }
            animator.SetBool("noteInRange", false);
        
        }
        return false;

    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if(!touchedInTime)
        failNote(collision.gameObject);


        touchedInTime = false;
    }
   
    public void failNote(GameObject note) {
        touchEffect.Stop();
        touchEffect.startColor = Color.red;
        touchEffect.Play();
        soundManager.PlaySEOverdrive(onFailNoteSound, 1);
        Destroy(note.gameObject);
        gameManager.background.ChangeToNormalMaterial();
        gameManager.RemoveNote();
        gameManager.ResetCombo();
        gameManager.AddScore(-1);
    }
   
}
