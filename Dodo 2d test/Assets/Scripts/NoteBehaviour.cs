﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteBehaviour : MonoBehaviour
{
    Rigidbody2D rigidbody;
   [SerializeField] private  float speed;
    public float score;
    public float minSpeed = 0.01f;
    public float maxSpeed = 0.1f;
    public float minScore = 200f;
    public float maxScore = 2000f;

    public GameManager gameManager;
    // Start is called before the first frame update
    void Start()
    {
        gameManager = FindObjectOfType<GameManager>();
        GetComponent<SpriteRenderer>().color = new Color(UnityEngine.Random.Range(0,1.01f), UnityEngine.Random.Range(0, 1.01f), UnityEngine.Random.Range(0, 1.01f),1);
         
        if (gameManager.easyMode) speed = UnityEngine.Random.Range(minSpeed,maxSpeed/2);
        else speed = UnityEngine.Random.Range(minSpeed,maxSpeed);
        if (gameManager.easyMode) score = UnityEngine.Random.Range(minScore/2, maxScore/2) * speed;
        else score = UnityEngine.Random.Range(200, 2000) * speed;
        rigidbody = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        rigidbody.AddForce(Vector3.down* speed,ForceMode2D.Force);
    }
}
