﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
   [SerializeField] float score;
    public Text scoreText;
    float totalNotes;
    public GameObject note;
    public GameObject[] spawners;
     public bool playerWin=false;
    float spawnedNotes;
    float maxScore=10000;
    public AudioClip bgm;
    public bool easyMode=true;
    public float squaresHitted;
    float precisionNumber;
    public Text precision;
    public int combo=0;
    public Text comboText;
    public BackgroundSelector background;
    // Start is called before the first frame update
    void Start()
    {
        FindObjectOfType<SoundManager>().PlayBGM(bgm);
        background = FindObjectOfType<BackgroundSelector>();
        UpdateText();
        StartCoroutine("SpawnNotes");
        
    }
    public void ChageMode() {
        easyMode = !easyMode;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void UpdateText() {
        scoreText.text = "SCORE: " + score;
        precisionNumber = squaresHitted / totalNotes * 100;
        Debug.Log(precisionNumber);
        precision.text =  precisionNumber  +  "%" ;
        if (precisionNumber >= 100)
        {
            background.ChangeToGold();
        }
       
        if (combo == 0)
        {
            comboText.text = "";
        }
        else if (combo % 20 == 0) {
            comboText.GetComponent<Animator>().Play("COMBO");
            Debug.Log("%%%%");
        }
        else comboText.text = "COMBO " + combo;

    }
     public void AddScore(float score) {
        if (score > 0) combo++;
        if (score <= maxScore)
        {if (easyMode) this.score += score / 2;
            else this.score += score;
            Debug.Log("Score added");
            UpdateText();
        }
        else { playerWin = true;
            StopCoroutine("SpawnNotes");
        }
         
    }
      IEnumerator SpawnNotes()
    {
        do
        {
            foreach (GameObject spawn in spawners)
            {
                GameObject g = Instantiate(note);
                g.transform.position = new Vector3 (spawn.transform.position.x,UnityEngine.Random.Range(spawn.transform.position.y, spawn.transform.position.y + 4),0);
                spawnedNotes++;
                totalNotes++;
            }
            if(easyMode)
                yield return new WaitForSeconds(0.6f);
            else
            yield return new WaitForSeconds(0.3f);
        }
        while (!playerWin && spawnedNotes != 0);
        
     
      
    }
    public void RemoveNote() {
        spawnedNotes--;
    }
    public void ResetCombo()
    {
        combo = 0;
         
    }
}
