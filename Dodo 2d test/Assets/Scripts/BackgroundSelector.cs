﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundSelector : MonoBehaviour
{
    Material normalMaterial;
   public  Material goldMaterial;
    public Material[] backgroundMaterials;
    // Start is called before the first frame update
    GameManager gameManager;
    void Start()
    {
        normalMaterial = GetComponent<MeshRenderer>().material;
        gameManager = FindObjectOfType<GameManager>();
        StartCoroutine("ChangeBackgroundColorRandomly");
    }

    // Update is called once per frame
    void Update()
    {
                
    }

    public void ChangeToGold() {
        GetComponent<MeshRenderer>().material = goldMaterial;


    }
    public void ChangeToNormalMaterial()
    {
        GetComponent<MeshRenderer>().material = normalMaterial;


    }
    int random;
    public IEnumerator ChangeBackgroundColorRandomly() {
        

        while (!gameManager.playerWin) {
            random = Random.Range(0, backgroundMaterials.Length);
            GetComponent<MeshRenderer>().material = backgroundMaterials[random];
            normalMaterial = GetComponent<MeshRenderer>().material;
            yield return new WaitForSeconds(12f);


        }

    }
}
